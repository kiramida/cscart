{** employees section **}

{capture name="mainbox"}

<form action="{""|fn_url}" method="post" id="employees_form" name="employees_form" enctype="multipart/form-data">
<input type="hidden" name="fake" value="1" />
{$employee_statuses=""|fn_get_default_statuses:true}
{$has_permission = fn_check_permissions("employee", "update_status", "admin", "POST")}

{if $employees}
    
{if $has_permission}
    {hook name="employees:bulk_edit"}
        {include file="addons/sd_staff/views/staff/components/bulk_edit.tpl"}
    {/hook}
{/if}
<div class="table-responsive-wrapper longtap-selection">
    <table class="table table-middle table--relative table-responsive">
    <thead>
    <tr>   
        <th width="6%" class="left mobile-hide">

            <input type="checkbox"
                class="bulkedit-toggler hide"
                data-ca-bulkedit-toggler="true"
                data-ca-bulkedit-disable="[data-ca-bulkedit-default-object=true]"
                data-ca-bulkedit-enable="[data-ca-bulkedit-expanded-object=true]"
            />
        </th>
        <th class="left mobile-hide" width="7%">{__("position_short")}</th>
        <th>{__("employee")}</th>

        <th width="6%" class="mobile-hide">&nbsp;</th>
        
        <th width="10%" class="right">{__("status")}</th>
    </tr>
    </thead>
    {foreach from=$employees item=employee}
    <tr class="cm-row-status-{$employee.status|lower} cm-longtap-target"
        {if $has_permission}
            data-ca-longtap-action="setCheckBox"
            data-ca-longtap-target="input.cm-item"
            data-ca-id="{$employee.employee_id}"
        {/if}
    >
        {$allow_save=$employee|fn_allow_save_object:"employees"}

        {if $allow_save}
            {$no_hide_input="cm-no-hide-input"}
        {else}
            {$no_hide_input=""}
        {/if}
        <td width="6%" class="left mobile-hide">
            <input type="checkbox" name="employee_ids[]" value="{$employee.employee_id}" class="cm-item {$no_hide_input} cm-item-status-{$employee.status|lower} hide" />
        </td>        
        <td width="7%" class="mobile-hide" data-th="{__("position_short")}">
            <input type="text" name="employees_data[{$employee.employee_id}][position]" size="3" maxlength="10" value="{$employee.position}" class="input-micro input-hidden" />
        </td>
        <td class="{$no_hide_input}" data-th="{__("employee")}">
            <a class="row-status" href="{"staff.update?employee_id=`$employee.employee_id`"|fn_url}">{$employee.first_name} {$employee.last_name}</a>
            {include file="views/companies/components/company_name.tpl" object=$employee}
        </td>
        
        <td width="6%" class="mobile-hide">
            {capture name="tools_list"}
                <li>{btn type="list" text=__("edit") href="staff.update?employee_id=`$employee.employee_id`"}</li>
            {if $allow_save}
                <li>{btn type="list" class="cm-confirm" text=__("delete") href="staff.delete?employee_id=`$employee.employee_id`" method="POST"}</li>
            {/if}
            {/capture}
            <div class="hidden-tools">
                {dropdown content=$smarty.capture.tools_list}
            </div>
        </td>
        <td width="10%" class="right" data-th="{__("status")}">
            {include file="common/select_popup.tpl" id=$employee.employee_id status=$employee.status hidden=true object_id_name="employee_id" table="employees" popup_additional_class="`$no_hide_input` dropleft"}
        </td>
    </tr>
    {/foreach}
    </table>
</div>
{else}
    <p class="no-items">{__("no_data")}</p>
{/if}
{capture name="adv_buttons"}
    {hook name="employees:adv_buttons"}    
    {include file="common/tools.tpl" tool_href="staff.add" prefix="top" hide_tools="true" title=__("add_employee") icon="icon-plus"}
    {/hook}
{/capture}
{capture name="buttons"}
    {include file="buttons/save.tpl" but_name="dispatch[staff.m_update]" but_role="action" but_target_form="employees_form" but_meta="cm-submit"}
{/capture}
</form>

{/capture}

{hook name="employees:manage_mainbox_params"}
    {$page_title = __("employees")}
{/hook}

{include file="common/mainbox.tpl" title=$page_title content=$smarty.capture.mainbox adv_buttons=$smarty.capture.adv_buttons buttons=$smarty.capture.buttons  sidebar=$smarty.capture.sidebar}

{** ad section **}