{if $employee}
    {assign var="id" value=$employee.employee_id}
{else}
    {assign var="id" value=0}
{/if}

{** employees section **}

{$allow_save = $employee|fn_allow_save_object:"employees"}
{$hide_inputs = ""|fn_check_form_permissions}

{capture name="mainbox"}

<form action="{""|fn_url}" method="post" class="form-horizontal form-edit{if !$allow_save || $hide_inputs} cm-hide-inputs{/if}" name="employees_form" enctype="multipart/form-data">
<input type="hidden" class="cm-no-hide-input" name="fake" value="1" />
<input type="hidden" class="cm-no-hide-input" name="employee_id" value="{$id}" />

{capture name="tabsbox"}

    <div id="content_general">
        {hook name="employees:general_content"}
        <div class="control-group">
            <label for="employee_name" class="control-label cm-required">{__("firstname")}</label>
            <div class="controls">
            <input type="text" name="employee_data[first_name]" id="employee_name" value="{$employee.first_name}" size="25" max="255" class="input-large" />
            </div>
        </div>
        <div class="control-group">
            <label for="employee_lastname" class="control-label cm-required">{__("lastname")}</label>
            <div class="controls">
            <input type="text" name="employee_data[last_name]" id="employee_lastname" value="{$employee.last_name}" size="25" max="255" class="input-large" />
            </div>
        </div>
        <div class="control-group">
            <label for="employee_function" class="control-label">{__("empl_function")}</label>
            <div class="controls">
            <input type="text" name="employee_data[function]" id="employee_function" value="{$employee.function}" size="25" max="255" class="input-large" />
            </div>
        </div>
        <div class="control-group">
            <label for="employee_email" class="control-label">{__("email")}</label>
            <div class="controls">
            <input type="text" name="employee_data[email]" id="employee_email" value="{$employee.email}" size="25" max="64" class="input-large" />
            </div>
        </div>        
        <div class="control-group" id="employee_text">
            <label class="control-label" for="employee_description">{__("description")}:</label>
            <div class="controls">
                <textarea id="employee_description" name="employee_data[description]" cols="35" rows="8" class="cm-wysiwyg input-large">{$employee.description}</textarea>
            </div>
        </div>
        <div class="control-group">
            <label for="elm_employee_position" class="control-label">{__("position_short")}</label>
            <div class="controls">
                <input type="text" name="employee_data[position]" id="elm_employee_position" value="{$employee.position|default:"0"}" size="3"/>
            </div>
        </div>

        {include file="common/select_status.tpl" input_name="employee_data[status]" id="elm_employee_status" obj_id=$id obj=$employee hidden=true}
        {/hook}
    <!--content_general--></div>
{/capture}

{include file="common/tabsbox.tpl" content=$smarty.capture.tabsbox active_tab=$smarty.request.selected_section track=true}

{capture name="buttons"}
    {if !$id}
        {include file="buttons/save_cancel.tpl" but_role="submit-link" but_target_form="employees_form" but_name="dispatch[staff.update]"}
    {else}
        {include file="buttons/save_cancel.tpl" but_name="dispatch[staff.update]" but_role="submit-link" but_target_form="employees_form" hide_first_button=$hide_first_button hide_second_button=$hide_second_button save=$id}
    {/if}
{/capture}

</form>

{/capture}

{include file="common/mainbox.tpl"
    title=($id) ? $employee.employee : __("employees.new_employee")
    content=$smarty.capture.mainbox
    buttons=$smarty.capture.buttons}

{** employee section **}
