{if $employees}   
<div class="table-wrapper">
    <table class="staff-table ">
    <thead>
    <tr>        
        <th>{__("employee")}</th>
        <th>{__("empl_function")}</th>
    </tr>
    </thead>
    {foreach from=$employees item=employee}
    {if $employee.status == 'A'} 
        <tr>
            <td data-th="{__("employee")}">
                <a class="" href="{"staff.view_employee?employee_id=`$employee.employee_id`"|fn_url}">{$employee.last_name} {$employee.first_name}</a>            
            </td>

            <td data-th="{__("function")}">
                <p>{$employee.function}</p>            
            </td>
        </tr>
    {/if}
    {/foreach}
    </table>
</div>
{else}
    <p class="no-items">{__("no_data")}</p>
{/if}
{capture name="mainbox_title"}{__("empl_list")}{/capture}