{if $employee && $employee.status == 'A'}   
<div class="table-wrapper">
    <table class="staff-table ">
        <tr>
            <th>
                {__("firstname")}        
            </th>
            <td data-th="{__("firstname")}">
                <p>{$employee.first_name} </p>            
            </td>
        </tr>
        <tr>
            <th>
                {__("lastname")}        
            </th>
            <td data-th="{__("lastname")}">
                <p>{$employee.last_name}</p>            
            </td>
        </tr>
        <tr>
            <th>
                {__("empl_function")}        
            </th>
            <td data-th="{__("empl_function")}">
                <p>{$employee.function} </p>            
            </td>
        </tr>
        <tr>
            <th>
                {__("email")}        
            </th>
            <td data-th="{__("email")}">
                <p>{$employee.email}</p>            
            </td>
        </tr>
        <tr>
            <th>
                {__("description")}        
            </th>
            <td data-th="{__("description")}">
                <p>{$employee.description nofilter}</p>            
            </td>
        </tr>
    </table>
</div>
{else}
    <p class="no-items">{__("no_data")}</p>
{/if}
{if $employee.status == 'A'} 
    {capture name="mainbox_title"}{__("employee")} {$employee.last_name} {$employee.first_name}{/capture}
{else}
    {capture name="mainbox_title"}{__("employee")}{/capture}
{/if}