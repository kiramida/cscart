<?php

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }
// Admin actions
if ($_SERVER['REQUEST_METHOD']	== 'POST') {
    fn_trusted_vars('employees', 'employee_data');
    $suffix = '';
    // Mass deleting of banners
    if ($mode == 'm_delete') {
        foreach ($_REQUEST['employee_ids'] as $v) {
            fn_delete_employee($v);
        }
        $suffix = '.manage';
    }    
    // Mass updating of employees 
    if ($mode == 'm_update') {
        foreach ($_REQUEST['employees_data'] as $id => $data) {            
                fn_update_employee($data, $id);            
        }
        $suffix = '.manage';
    }
    // Mass updating of statuses
    if (
        $mode === 'm_update_statuses'
        && !empty($_REQUEST['employee_ids'])
        && is_array($_REQUEST['employee_ids'])
        && !empty($_REQUEST['status'])
    ) {
        $status_to = (string) $_REQUEST['status'];
        foreach ($_REQUEST['employee_ids'] as $employee_id) {
            fn_tools_update_status([
                'table'             => 'employees',
                'status'            => $status_to,
                'id_name'           => 'employee_id',
                'id'                => $employee_id,
                'show_error_notice' => false
            ]);
        }
        // Update status by Ajax
        if (defined('AJAX_REQUEST')) {
            $redirect_url = fn_url('staff.manage');
            if (isset($_REQUEST['redirect_url'])) {
                $redirect_url = $_REQUEST['redirect_url'];
            }
            Tygh::$app['ajax']->assign('force_redirection', $redirect_url);
            Tygh::$app['ajax']->assign('non_ajax_notifications', true);
            return [CONTROLLER_STATUS_NO_CONTENT];
        }
    }
    // Add/edit employees
    if ($mode == 'update') {
        $employee_id = fn_update_employee($_REQUEST['employee_data'], $_REQUEST['employee_id']);
        $suffix = ".update?employee_id=$employee_id";
    }
    // Deleting of employee
    if ($mode == 'delete') {
        if (!empty($_REQUEST['employee_id'])) {
            fn_delete_employee($_REQUEST['employee_id']);
        }
        $suffix = '.manage';
    }
    return array(CONTROLLER_STATUS_OK, 'staff' . $suffix);
}
//page for editing an employee
if ($mode == 'update') {
    $employee = fn_get_employee($_REQUEST['employee_id']);    
    Registry::set('navigation.tabs', array (
        'general' => array (
            'title' => __('general'),
            'js' => true
        ),
    ));
    Tygh::$app['view']->assign('employee', $employee);
} 
//page with list of employees
elseif ($mode == 'manage') {
    $employees = fn_get_staff();   
    fn_add_breadcrumb(__('staff'));
    Tygh::$app['view']->assign(array(
        'employees'  => $employees,
    ));
}


