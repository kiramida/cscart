<?php

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }
if($mode == 'view') {
    $employees = fn_get_staff();    
    Tygh::$app['view']->assign(array(
        'employees'  => $employees,
    ));
}
else if($mode == 'view_employee') {
    $employee = fn_get_employee($_REQUEST['employee_id']);    
    Tygh::$app['view']->assign(array(
        'employee'  => $employee,
    ));
}