<?php

use Tygh\Registry;

$schema['central']['website']['items']['sd_staff'] = [
    'attrs' => [
        'class'=>'is-addon'
    ],
    'href' => 'staff.manage',
    'alt' => 'staff.manage',
    'position' => 40
];

return $schema;
