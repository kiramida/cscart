<?php

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

/**
 * Gets employees list  
 *
 * @return array Employees list 
 */
function fn_get_staff()
{ 
    $fields = array (
        '?:employees.employee_id',
        '?:employees.status',
        '?:employees.first_name',
        '?:employees.last_name',
        '?:employees.function',
        '?:employees.email',
        '?:employees.description',
        '?:employees.position',
    );    
    $employees = db_get_hash_array(
        "SELECT ?p FROM ?:employees "
      . "ORDER BY position",
        'employee_id', implode(', ', $fields)
    );
    
    return $employees;
}

/**
 * Gets employee by id
 * 
 * @param int $employee_id Employee id  
 *
 * @return array Employees list 
 */
function fn_get_employee($employee_id)
{
    // Unset SQL variable
    $condition = '';
    $fields = array (
        '?:employees.employee_id',
        '?:employees.status',
        '?:employees.first_name',
        '?:employees.last_name',
        '?:employees.function',
        '?:employees.email',
        '?:employees.description',
        '?:employees.position',
    );
    $condition = db_quote("WHERE ?:employees.employee_id = ?i", $employee_id);
    $employee = db_get_row("SELECT " . implode(", ", $fields) . " FROM ?:employees $condition");

    return $employee;
}

/**
 * Updates employee data by id
 * 
 * @param array $data         New employee data to set 
 * @param int   $employee_id  Employee id  
 *
 * @return array Employees list 
 */
function fn_update_employee($data, $employee_id)
{
    if (!empty($employee_id)) {
        db_query("UPDATE ?:employees SET ?u WHERE employee_id = ?i", $data, $employee_id);        
    } else {
        $employee_id = $data['employee_id'] = db_query("REPLACE INTO ?:employees ?e", $data);
    }
    return $employee_id;
}

/**
 * Deletes employee by id
 *
 * @param int   $employee_id Employee id 
 * 
 * @return void
 */
function fn_delete_employee($employee_id)
{
    if (!empty($employee_id)) {
        db_query("DELETE FROM ?:employees WHERE employee_id = ?i", $employee_id);        
    }
}
